package com.tsc.smironova.tm.model;

import com.tsc.smironova.tm.util.ColorUtil;

public class Command {

    private final String name;
    private final String argument;
    private final String description;

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getArgument() {
        return argument;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty())
            result += ColorUtil.PURPLE + name + ColorUtil.RESET + " ";
        if (argument != null && !argument.isEmpty())
            result += ColorUtil.PURPLE + "(" + argument + ") " + ColorUtil.RESET;
        if (description != null && !description.isEmpty())
            result += "- " + description + " ";
        return result;
    }

}
