package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.IProjectRepository;
import com.tsc.smironova.tm.api.service.IProjectService;
import com.tsc.smironova.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty() || description == null || description.isEmpty())
            return null;
        final Project project = new Project(name, description);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null)
            return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null)
            return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index == null || index < 0)
            return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(Integer index) {
        if (index == null || index < 0)
            return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty() || name == null || name.isEmpty())
            return null;
        final Project project = findOneById(id);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || name == null || name.isEmpty())
            return null;
        final Project project = findOneByIndex(index);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
