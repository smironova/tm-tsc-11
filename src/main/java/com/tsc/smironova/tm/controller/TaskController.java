package com.tsc.smironova.tm.controller;

import com.tsc.smironova.tm.api.controller.ITaskController;
import com.tsc.smironova.tm.api.service.ITaskService;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void showTask(final Task task) {
        if (task == null)
            return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        showTask(task);
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        showTask(task);
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        showTask(task);
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        showTask(task);
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        showTask(task);
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        showTask(task);
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdatedId = taskService.updateTaskById(id, name, description);
        if (taskUpdatedId == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdatedIndex = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdatedIndex == null) {
            System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
            return;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

}
