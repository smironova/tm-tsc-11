package com.tsc.smironova.tm.api.repository;

import com.tsc.smironova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
