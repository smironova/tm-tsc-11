package com.tsc.smironova.tm.api.controller;

import com.tsc.smironova.tm.model.Project;

public interface IProjectController {

    void showProjectList();

    void createProject();

    void clearProject();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

}
