package com.tsc.smironova.tm.api.controller;

import com.tsc.smironova.tm.model.Task;

public interface ITaskController {

    void showTaskList();

    void createTask();

    void clearTask();

    void showTask(Task task);

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

}
